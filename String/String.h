#pragma once

#include <iostream>
#include <vector>

class String
{
public:
	String() = default;
	String(size_t size, char character) noexcept;
	String(const char*) noexcept;
	String(const String&) noexcept;

	String& operator=(const String&);
	const char& operator[](size_t) const;
	char& operator[](size_t);
	bool operator<(const String&);
	bool operator<=(const String&);
	bool operator>(const String&);
	bool operator>=(const String&);
	bool operator==(const String&);
	bool operator!=(const String&);
	String operator+(const String&) const;
	String& operator+=(const String&);
	String operator*(int n) const;

	const char& Front() const;
	char& Front();
	const char& Back() const;
	char& Back();

	inline void Clear() { _size = 0; };

	void PushBack(char);
	void PopBack();

	void Resize(size_t);
	void Resize(size_t, char);

	void Reserve(size_t);

	void ShrinkToFit();

	void Swap(String&);

	inline bool Empty() const noexcept { return _size; };
	inline size_t Size() const noexcept { return _size; };
	inline size_t Capacity() const noexcept { return _capacity; };
	
	inline char* Head() const { return _head; };

	String SubStr(char*, char*) const;
	std::vector<String> Split(const String& delim = " ");
	String Join(const std::vector<String>& strings);
	~String();

private:
	size_t _size = 0;
	size_t _capacity = 0;
	char* _head = nullptr;
};

std::ostream& operator<<(std::ostream& os, const String&);
std::istream& operator>>(std::istream& is, String&);
