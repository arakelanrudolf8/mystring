#include "String.h"

#include <cstring>

template<typename T>
const T& min(const T& left, const T& right)
{
	return left < right ? left : right;
}

template<typename T>
const T& max(const T& left, const T& right)
{
	return left > right ? left : right;
}

String::String(size_t size, char character) noexcept :
	_size(size),
	_capacity(size)
{
	_head = new char[_size];
	char* curr;
	for (curr = _head; curr < _head + _size; ++curr)
		*curr = character;
}

String::String(const char* cstr) noexcept :
	_size(strlen(cstr)),
	_capacity(strlen(cstr))
{
	_head = new char[_size];
	char* curr;
	for (curr = _head; curr < _head + _size; ++curr, ++cstr)
		*curr = *cstr;
}

String::String(const String& other) noexcept
{
	*this = other;
}

String& String::operator=(const String& other)
{
	if (_capacity == other._capacity && _size == other._size && _head == other._head)
		return *this;

	delete[] _head;
	_capacity = other._capacity;
	_size = other._size;
	_head = new char[_capacity];

	char* curr, * pOther;
	for (curr = _head, pOther = other._head; curr < _head + _size; ++curr, ++pOther)
		*curr = *pOther;

	return *this;
}

const char& String::operator[](size_t index) const
{
	if (index < _size)
		return _head[index];
	else
		throw("out of range error");
}

char& String::operator[](size_t index) 
{
	if (index < _capacity)
		return _head[index];
	else
		throw("out of range error");
}

bool String::operator<(const String& other)
{
	for (auto* curr = _head, * otherCurr = other._head; curr < _head + min(_size, other._size); ++curr, ++otherCurr)
	{
		if (*curr < *otherCurr)
			return true;
		else if (*curr > *otherCurr)
			return false;
	}

	return _size < other._size;
}

bool String::operator<=(const String& other)
{
	for (auto* curr = _head, *otherCurr = other._head; curr < _head + min(_size, other._size); ++curr, ++otherCurr)
	{
		if (*curr < *otherCurr)
			return true;
		else if (*curr > *otherCurr)
			return false;
	}

	return _size <= other._size;
}

bool String::operator>(const String& other)
{
	for (auto* curr = _head, *otherCurr = other._head; curr < _head + min(_size, other._size); ++curr, ++otherCurr)
	{
		if (*curr > *otherCurr)
			return true;
		else if (*curr < *otherCurr)
			return false;
	}

	return _size > other._size;
}

bool String::operator>=(const String& other)
{
	for (auto* curr = _head, *otherCurr = other._head; curr < _head + min(_size, other._size); ++curr, ++otherCurr)
	{
		if (*curr > *otherCurr)
			return true;
		else if (*curr < *otherCurr)
			return false;
	}

	return _size >= other._size;
}

bool String::operator==(const String& other)
{
	if (_size != other._size)
		return false;

	for (auto* curr = _head, *otherCurr = other._head; curr < _head + _size; ++curr, ++otherCurr)
	{
		if (*curr != *otherCurr)
			return false;
	}

	return true;
}

bool String::operator!=(const String& other)
{
	return !(*this == other);
}

String String::operator+(const String& other) const
{
	String s;
	s.Resize(_size + other._size);
	char* sCurr = s._head;
	for (auto* curr = _head; curr < _head + _size; ++curr, ++sCurr)
		*sCurr = *curr;
	for (auto* curr = other._head; curr < other._head + other._size; ++curr, ++sCurr)
		*sCurr = *curr;

	return s;
}

String& String::operator+=(const String& other)
{
	if (_capacity < _size + other._size)
		Reserve(_size + other._size);

	char* curr = &_head[_size];
	for (auto* otherCurr = other._head; otherCurr < other._head + other._size; ++curr, ++otherCurr)
		*curr = *otherCurr;

	_size += other._size;

	return *this;
}

String String::operator*(int n) const
{
	String s;
	s.Reserve(_size * n);
	for (int i = 0; i < n; ++i)
		s += *this;

	return s;
}

std::ostream& operator<<(std::ostream& os, const String& s) 
{
	for (char* curr = s.Head(); curr < s.Head() + s.Size(); ++curr)
		os << *curr;

	return os;
}

std::istream& operator>>(std::istream& is, String& s) 
{
	for (char* curr = s.Head(); curr < s.Head() + s.Size(); ++curr)
		is >> *curr;

	return is;
}

const char& String::Front() const
{
	return _head[0];
}

char& String::Front() 
{
	return _head[0];
}

const char& String::Back() const
{
	return _head[_size];
}

char& String::Back()
{
	return _head[_size];
}

void String::PushBack(char character)
{
	if (_size == _capacity)
	{
		_capacity *= 2;
		char* newHead = new char[_capacity];
		char* oldCurr, * newCurr;
		for (oldCurr = _head, newCurr = newHead; oldCurr < _head + _size; ++oldCurr, ++newCurr)
			*newCurr = *oldCurr;
		
		delete[] _head;
		_head = newHead;
	}

	_head[_size] = character;
	++_size;
}

void String::PopBack()
{
	if (!_size)
		return;

	--_size;
}

void String::Resize(size_t new_size)
{
	if (new_size > _capacity)
	{
		_capacity = new_size;
		char* newHead = new char[_capacity];
		char* oldCurr, * newCurr;
		for (oldCurr = _head, newCurr = newHead; oldCurr < _head + _size; ++oldCurr, ++newCurr)
			*newCurr = *oldCurr;

		delete[] _head;
		_head = newHead;
	}

	_size = new_size;
}

void String::Resize(size_t new_size, char character)
{
	size_t oldSize = _size;
	Resize(new_size);

	if (new_size > oldSize)
	{ 
		char* curr;
		for (curr = _head + oldSize; curr < _head + _size; ++curr)
			*curr = character;
	}
}

void String::Reserve(size_t new_cap)
{
	if (new_cap <= _capacity)
		return;

	_capacity = new_cap;
	char* newHead = new char[_capacity];
	char* oldCurr, * newCurr;
	for (oldCurr = _head, newCurr = newHead; oldCurr < _head + _size; ++oldCurr, ++newCurr)
		*newCurr = *oldCurr;

	delete[] _head;
	_head = newHead;
}

void String::ShrinkToFit()
{
	if (_capacity == _size)
		return;

	_capacity = _size;
	char* newHead = new char[_capacity];
	char* oldCurr, * newCurr;
	for (oldCurr = _head, newCurr = newHead; oldCurr < _head + _size; ++oldCurr, ++newCurr)
		*newCurr = *oldCurr;

	delete[] _head;
	_head = newHead;
}

void String::Swap(String& other)
{
	char* tempHead = _head;
	_head = other._head;
	other._head = tempHead;
	
	size_t tempSize = _size;
	_size = other._size;
	other._size = tempSize;

	if (_capacity < _size)
		_capacity = _size;
	if (other._capacity < other._size)
		other._capacity = other._size;
}

char* findStr(char* source, char* end, const String& target)
{
	for (char* s = source, * t = target.Head(); s < end; ++s, ++t)
	{
		if (*s != *t)
			t = target.Head() - 1;
		else if (t == target.Head() + target.Size() - 1)
		{
			return s - target.Size() + 1;
		}
	}

	return end;
}

String String::SubStr(char* start, char* end) const
{
	String s;
	s.Reserve(end - start);
	for (char* curr = start; curr < end; ++curr)
		s.PushBack(*curr);

	return s;
}

std::vector<String> String::Split(const String& delim)
{
	std::vector<String> result;
	for (char* curr = _head; curr < _head + _size;)
	{
		char* pos = findStr(curr, _head + _size, delim);
		if (pos < _head + _size)
		{
			result.push_back(SubStr(curr, pos - delim.Size() + 1));
			curr = pos + delim.Size();
		}
		else
			curr = pos;
	}

	return result;
}

String String::Join(const std::vector<String>& strings)
{
	String s;
	for (auto it = strings.begin(); it != strings.end(); ++it)
	{
		s += *it;
		if (it + 1 < strings.end())
			s += *this;
	}

	return s;
}

String::~String()
{
	delete[] _head;
}

int main()
{
	String a, b;
	a = "abc";
	b = "abc";

	std::vector<String> v = {"adsadsa"};
	std::cout << (a != b);
}